<div class="page clearfix">

  <?php if ($page['header_top']): ?>
  <div class="header-top">
    <div class="container-16 clearfix">
      <div class="nav lato-bold grid-16">
        <?php print render($page['header_top']); ?>
      </div>
    </div>
  </div><!-- /.header-top -->
  <?php endif; ?>

  <div class="main-container container-16 clearfix">
    
    <header class="header">
      <?php if ($page['header']): ?>
      <div class="header-region grid-16 clearfix">
        <?php print render($page['header']); ?>
      </div>
      <?php endif; ?>

      <div class="branding">
        <?php if ($linked_logo_img): ?>
        <div class="logo grid-8 prefix-4 suffix-4"><?php print $linked_logo_img; ?></div>
        <?php endif; ?>
        <?php if ($linked_site_name): ?>
        <h1 class="site-name element-invisible"><?php print $linked_site_name; ?></h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
        <div class="site-slogan"><?php print $site_slogan; ?></div>
        <?php endif; ?>
      </div>
    </header><!-- /.header -->

    <nav class="primary-navigation nav lato-bold grid-16">
      <?php print $main_menu_links; ?>
    </nav><!-- /.navigation -->

    <div class="main clearfix">
      
      <?php if ($page['highlighted']): ?>
      <div class="highlighted grid-16 clearfix">
        <?php print render($page['highlighted']); ?>
      </div>
      <?php endif; ?>

      <div class="grid-11">
        <?php print $breadcrumb; ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
          <h1 class="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>      
        <?php if ($tabs): ?>
          <?php print render($tabs); ?>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php print render($page['help']); ?>

        <?php if($page['content_top']): ?>
        <div class="content-top clearfix">
          <?php print render($page['content_top']); ?>
        </div><!-- ./content-top -->
        <?php endif; ?>

        <div id="main-content" class="main-content clearfix">
          <?php print render($page['content']); ?>
        </div><!-- /.content -->

        <?php if($page['content_bottom']): ?>
        <div class="content-bottom clearfix">
          <?php print render($page['content_bottom']); ?>
        </div><!-- /.content-bottom -->
        <?php endif; ?>


      </div><!-- /.main -->

      <?php if ($page['sidebar_first']): ?>
        <div class="sidebar sidebar-first grid-4 suffix-1">
          <?php print render($page['sidebar_first']); ?>
        </div><!-- /.sidebar-first -->
      <?php endif; ?>

    </div><!-- /.main -->

    <footer class="footer-container grid-16 clearfix">
      <?php if ($page['footer_top']): ?>
      <div class="footer-top clearfix">
        <?php print render($page['footer_top']); ?>
      </div>
      <?php endif; ?>

      <?php if ($page['footer']): ?>
      <div class="footer clearfix">
        <?php print render($page['footer']); ?>
      </div>
      <?php endif; ?>

      <?php if ($page['footer_bottom']): ?>
      <div class="footer-bottom clearfix">
        <?php print render($page['footer_bottom']); ?>
      </div>
      <?php endif; ?>

      <?php print $feed_icons; ?>
    </footer>

  </div><!-- /.main-container, .container-16 -->

</div><!-- /.page -->
